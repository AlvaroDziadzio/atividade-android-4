package com.example.transmitter

import android.Manifest
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.karumi.dexter.Dexter
import com.karumi.dexter.listener.single.DialogOnDeniedPermissionListener




class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun OpenImage(view: View) {
        val intent = Intent(this, ShareImageActivity::class.java)
        startActivity(intent)
    }

    fun OpenText(view: View) {
        val intent = Intent(this, ShareTextActivity::class.java)
        startActivity(intent)
    }

    fun permission(view: View) {

        val dialogPermissionListener = DialogOnDeniedPermissionListener.Builder
            .withContext(this)
            .withTitle("Camera permission")
            .withMessage("Camera permission is needed to take pictures of your cat")
            .withButtonText(android.R.string.ok)
            .withIcon(R.mipmap.ic_launcher)
            .build()

        Dexter.withActivity(this)
            .withPermission(Manifest.permission.CAMERA)
            .withListener( dialogPermissionListener).check()

    }
}
