package com.example.transmitter

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_share_text.*

class ShareTextActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_text)
    }

    fun share(view: View) {
        val int = Intent(Intent.ACTION_SEND)
        int.putExtra(Intent.EXTRA_TEXT, content.text.toString())
        int.type = "text/plain"
        if (int.resolveActivity(packageManager) != null)
            startActivity(int)
    }
}
