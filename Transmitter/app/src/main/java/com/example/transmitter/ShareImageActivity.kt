package com.example.transmitter

import android.app.Activity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity;

import kotlinx.android.synthetic.main.activity_share_image.*
import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.Toast


class ShareImageActivity : AppCompatActivity() {

    var fileUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_image)

    }

    fun share(view: View) {
        if (fileUri != null) {
            val int = Intent(Intent.ACTION_SEND)
            int.type = "image/*"
            int.putExtra(Intent.EXTRA_STREAM, this.fileUri)
            startActivity(int)
        }
        else {
            Toast.makeText(this, "No image Selected", Toast.LENGTH_SHORT)
        }
    }

    fun select(view: View) {
        val int = Intent(Intent.ACTION_PICK)
        int.type = "image/*"
        startActivityForResult(int, 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {

            fileUri = data!!.data
            preview.setImageURI(fileUri)

        }
    }

}
